#!/usr/bin/make

DOCKER=docker
SECRET_FILE=.secret
RANDOM_DEV=/dev/urandom
LOCAL_SECRET=$(shell cat $(SECRET_FILE) || (dd if=$(RANDOM_DEV) of=/dev/stdout bs=512 count=1 | \
                tr -c -d [a-z] | head -c 10 > $(SECRET_FILE); cat $(SECRET_FILE)))
BASE_IMAGE=debian:testing
TIMESTAMP=$(shell date "+%Y%m%d%H%M")

console-box:
	$(DOCKER) build --build-arg "BASE_IMAGE=$(BASE_IMAGE)" \
	    --tag "$(LOCAL_SECRET)-console:$(TIMESTAMP)" console-box
	$(DOCKER) tag "$(LOCAL_SECRET)-console:$(TIMESTAMP)" "$(LOCAL_SECRET)-console:latest"

dev-box: console-box
	$(DOCKER) build --build-arg "LOCAL_SECRET=$(LOCAL_SECRET)" \
	    --tag "$(LOCAL_SECRET)-dev:$(TIMESTAMP)" dev-box
	$(DOCKER) tag "$(LOCAL_SECRET)-dev:$(TIMESTAMP)" "$(LOCAL_SECRET)-dev:latest"

.PHONY: console-box dev-box